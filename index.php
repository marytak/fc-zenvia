<!DOCTYPE HTML>
<html>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>FCamara</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <style>
        * {
            padding:0;
            margin: 0;
        }

        .choice {
            text-align: center;
            padding: 50px 0;
        }

        .choice li {
            display: inline-block;
            margin: 0 50px;
        }
    </style>

</head>

<body>

    <ul class="choice">
        <li>
            <a href="drogaraia/index.html">
                <img src="drogaraia.png" alt="Droga Raia" title="Droga Raia">
            </a>
        </li>
        <!--li>
            <a href="drogasil/index.html">
                <img src="drogasil.png" alt="Drogasil" title="Drogasil">
            </a>
        </li-->
    </ul>

</body>

</html>